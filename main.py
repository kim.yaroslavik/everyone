from pyrogram import Client
from pyrogram import filters

app = Client("my_account")

@app.on_message(filters.me & filters.regex("@everyone"))
def sending(client, message):
    text = ""
    currentUserCounter = 0
    chatID = message.chat.id
    # Storing message IDs for deleting afterwards.
    messageIDs = []
    for member in app.iter_chat_members(message.chat.id):
        # Sending ping every 5 users. Cleaning the text afterwards.
        if currentUserCounter % 5 == 0 and currentUserCounter != 0:
            msgID = app.send_message(chatID, text).message_id
            messageIDs.append(msgID)
            text = ""
        if member.user.username != None:
            text += "@" + member.user.username + " "
        elif member.user.first_name != None:
            text += "<a href=\"tg://user?id=" + str(member.user.id) + "\">" + member.user.first_name + "</a> "
        else:
            text += "<a href=\"tg://user?id=" + str(member.user.id) + "\">Deleted</a> "
        currentUserCounter += 1
    # Sending last pings
    msgID = app.send_message(chatID, text).message_id
    messageIDs.append(msgID)
    # Deleting all sent messages to cover up
    app.delete_messages(chatID, messageIDs)

app.run()
